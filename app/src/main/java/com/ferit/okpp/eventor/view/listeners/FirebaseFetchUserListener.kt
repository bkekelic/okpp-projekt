package com.ferit.okpp.eventor.view.listeners

import com.ferit.okpp.eventor.model.User

interface FirebaseFetchUserListener {
    fun onSuccessUserFetch(user: User)
    fun onFailureUserFetch(message: String)
}