package com.ferit.okpp.eventor.view.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ferit.okpp.eventor.view.fragments.profile.ProfileOnGoingEvents
import com.ferit.okpp.eventor.view.fragments.profile.ProfilePastEvents

class ProfileViewPagerAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {

    private val fragments = arrayOf(
        ProfileOnGoingEvents.newInstance(),
        ProfilePastEvents.newInstance()
    )

    private val titles = arrayOf("Future events", "Past events")

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

    override fun getCount(): Int  = fragments.size
}