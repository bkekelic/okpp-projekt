package com.ferit.okpp.eventor.DataAccess

import android.util.Log
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.MyPreference
import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.view.listeners.FetchAllEventGuestsListener
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchGuestsListener
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchUserListener
import com.ferit.okpp.eventor.view.listeners.ReadDataListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class UserInformation {

    lateinit var guestList: MutableList<User>

    fun getUserInformation(usersId: MutableList<String>, fetchAllGuestsListener: FetchAllEventGuestsListener) {
        val ref = FirebaseDatabase.getInstance().getReference("users")

        guestList = mutableListOf()

        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                for(e in p0.children) {
                    var userInfo = e.getValue(User::class.java)

                    for(ID in usersId) {
                        if(userInfo!!.id == ID) {
                            Log.d("EventDetails", userInfo.name)
                            guestList.add(userInfo)
                        }
                    }
                }
                fetchAllGuestsListener.onSuccessGuestsFetch(guestList)
            }

        })
    }
}