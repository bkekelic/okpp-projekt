package com.ferit.okpp.eventor.view.fragments.home


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.MyPreference

import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.repository.GuestsRepository
import com.ferit.okpp.eventor.utils.showFragment
import com.ferit.okpp.eventor.view.adapters.GuestListAdapter
import com.ferit.okpp.eventor.view.fragments.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_home_page.*
import kotlinx.android.synthetic.main.fragment_guests_dialog.*


class GuestsDialog : DialogFragment() {

    private val guestListAdapter by lazy { GuestListAdapter(::onGuestClicked) }
    private var guests = mutableListOf<User>()
    private val pref = MyPreference(MyApplication.ApplicationContext)

    private val guestsRepository = GuestsRepository()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_guests_dialog, container, false)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        guests.clear()
        guests = guestsRepository.getGuestList()
        guestListRecyclerView.layoutManager = LinearLayoutManager(context)
        guestListRecyclerView.adapter = guestListAdapter
        guestListAdapter.setGuests(guests)
    }

    private fun onGuestClicked(guest: User){
        pref.setUserProfileId(guest.id)
        activity?.showFragment(R.id.eventsFragmentContainer, ProfileFragment.newInstance(guest.id))
    }

    companion object{
        fun newInstance(): GuestsDialog {
            return GuestsDialog()
        }
    }
}
