package com.ferit.okpp.eventor.DataAccess

import android.util.Log
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.view.listeners.ReadDataListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_add_event.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class ReserveASeat {

    fun reserveASeat(eventId: String, userId: String) {
        val ref = FirebaseDatabase.getInstance().getReference("events/$eventId/guests/${userId}")
        Log.d("EventDetails", userId.toString())

        ref.setValue(userId)
            .addOnSuccessListener {
                Log.d("AddEvent", "Event successfully saved to Firebase.")
            }
            .addOnFailureListener{
                Log.d("AddEvent", "Failed to save event to Firebase.")
            }
    }
}