package com.ferit.okpp.eventor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.ferit.okpp.eventor.view.activities.HomePageActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    lateinit var email: String
    lateinit var password: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginButton.setOnClickListener{
            login()
        }

        dontHaveAnAccount.setOnClickListener {
            goToRegistartion()
        }
    }

    private fun login() {
        email = emailLogin.text.toString()
        password = passwordLogin.text.toString()

        if(email.isEmpty() && password.isEmpty()) {
            Toast.makeText(this,"Please, enter your email or password!", Toast.LENGTH_LONG).show()
            return
        }

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener() {
                if(!it.isSuccessful) {
                    return@addOnCompleteListener
                }

                Log.d("LoginActivity", "User is successfully logged in with uid: ${it.result?.user?.uid}")
                logIn()
            }
            .addOnFailureListener{
                Log.d("RegistratonActivity", "Failed to log in: ${it.message}")
                Toast.makeText(this,"Failed to log in: ${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    private fun logIn() {
        val intent = Intent(this, HomePageActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK) //pritiskom na back button vodi van iz aplikacije, a ne na RegistrationActivity
        startActivity(intent)
    }

    private fun goToRegistartion() {
        val intent = Intent(this, RegistrationActivity::class.java)
        startActivity(intent)
    }
}
