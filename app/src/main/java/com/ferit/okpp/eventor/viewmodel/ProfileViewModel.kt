package com.ferit.okpp.eventor.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.ferit.okpp.eventor.model.Event
import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.repository.ProfileRepository
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchUserListener
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchUserOnGoingEventsListener
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchUserPastEventsListener
import com.ferit.okpp.eventor.view.listeners.FirebaseUpdateUserListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.chrono.ChronoLocalDate
import java.time.chrono.ChronoLocalDateTime
import java.util.*
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.MyPreference
import com.ferit.okpp.eventor.view.fragments.profile.ProfileFragment


class ProfileViewModel : ViewModel() {

    private var repository: ProfileRepository = ProfileRepository()
    var firebaseFetchUserListener: FirebaseFetchUserListener? = null
    var firebaseUpdateUserListener: FirebaseUpdateUserListener? = null
    var firebaseFetchUserOnGoingEventsListener: FirebaseFetchUserOnGoingEventsListener? = null
    var firebaseFetchUserPastEventsListener: FirebaseFetchUserPastEventsListener? = null
    private val disposables = CompositeDisposable()
    private val currentUserId = MyPreference(MyApplication.ApplicationContext).getUserProfileId()

    fun getUserDetails(userId: String) {
        disposables.add(
            repository.getUserDetails(userId)
                .subscribeOn(Schedulers.io())
                .take(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    //sending a success callback
                    firebaseFetchUserListener?.onSuccessUserFetch(it)
                }, {
                    //sending a failure callback
                    firebaseFetchUserListener?.onFailureUserFetch(it.message!!)
                })
        )
    }

    fun updateUser(user: User) {
        disposables.add(
            repository.updateUser(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    firebaseUpdateUserListener?.onSuccessUserUpdate()
                }, {
                    firebaseUpdateUserListener?.onFailureUserUpdate(
                        it.message ?: "Error while updating"
                    )
                })
        )
    }

    private fun checkIfEventDateIsAfterNow(date: String): Boolean {
        val eventDate = date.split("-").toTypedArray()
        val currentDate = LocalDate.now()

        if (eventDate[0].toInt() > currentDate.year) return true
        if (eventDate[1].toInt() > currentDate.monthValue) return true
        if (eventDate[2].toInt() >= currentDate.dayOfMonth) return true

        return false
    }

    fun getUserOnGoingEvents() {

        disposables.add(
            repository.getUserOnGoingEvents(currentUserId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    for (guest in it.guests) {
                        if (guest.value == currentUserId && checkIfEventDateIsAfterNow(it.dateStarting)) {
                            firebaseFetchUserOnGoingEventsListener?.onSuccessNextUserOnGoingEventFetch(
                                it
                            )
                        }
                    }
                }, {
                    firebaseFetchUserOnGoingEventsListener?.onFailureUserOnGoingEventsFetch(
                        it.message ?: "Can't fetch events"
                    )
                }, {
                    firebaseFetchUserOnGoingEventsListener?.onCompleteOnGoingEventsFetch()
                })
        )
    }

    fun getUserPastEvents() {
        disposables.add(
            repository.getUserPastEvents(currentUserId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    for (guest in it.guests) {
                        if (guest.value == currentUserId && !checkIfEventDateIsAfterNow(it.dateStarting)) {
                            firebaseFetchUserPastEventsListener?.onSuccessNextUserPastEventFetch(it)
                        }
                    }
                }, {
                    firebaseFetchUserPastEventsListener?.onFailureUserPastEventsFetch(
                        it.message ?: "Can't fetch events"
                    )
                },{
                    firebaseFetchUserPastEventsListener?.onCompletePastEventsFetch()
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

}