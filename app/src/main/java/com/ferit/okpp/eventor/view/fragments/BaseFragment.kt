package com.ferit.okpp.eventor.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.view.activities.BaseActivity

abstract class BaseFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutResourceId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as BaseActivity).supportActionBar?.title = setUpTitle()
        val toolbar: Toolbar = activity!!.findViewById(R.id.toolbar)

        val iconId = setUpNavigationIcon()
        if (iconId == null) toolbar.navigationIcon = null
        else toolbar.setNavigationIcon(iconId)

        toolbar.setNavigationOnClickListener { fragmentManager!!.popBackStack() }

        setupUi()
        setOnClickListeners()
    }

    abstract fun getLayoutResourceId(): Int
    abstract fun setupUi()
    abstract fun setOnClickListeners()
    abstract fun setUpTitle(): String

    // Send null if don't need to show navigation icon
    abstract fun setUpNavigationIcon(): Int?
}