package com.ferit.okpp.eventor.DataAccess

import android.util.Log
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.MyPreference
import com.ferit.okpp.eventor.model.Event
import com.ferit.okpp.eventor.repository.HomePageRepository
import com.ferit.okpp.eventor.utils.getDateAsString
import com.ferit.okpp.eventor.utils.getLocalDateAsString
import com.ferit.okpp.eventor.view.listeners.ReadDataListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.sql.Date
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class UpcomongEvents {
    lateinit var listOfEvents: MutableList<EventDB>
    private val eventRepository = HomePageRepository()

    fun getUpcomingEvents(readData: ReadDataListener) {
        val ref = FirebaseDatabase.getInstance().getReference("events")

        ref.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                listOfEvents = mutableListOf()
                for(e in p0.children) {
                    var events = e.getValue(EventDB::class.java)

                    val date: LocalDate = LocalDate.now()

                    //getLocalDateAsString(date)

                    val eventDate = LocalDate.parse(events!!.dateStarting, DateTimeFormatter.ISO_DATE)

                    Log.d("DateMy", date.toString())
                    Log.d("DateMy", eventDate.toString())

                    if(date.isBefore(eventDate)) {
                        Log.d("UpcomingEvents", "veci je")
                        listOfEvents.add(events!!)
                    }
                    //Log.d("UpcomingEvents", events.dateStarting)
                }

                readData.readData(listOfEvents)

                val pref = MyPreference(MyApplication.ApplicationContext)
                pref.setEvents(listOfEvents)
                Log.d("+++", listOfEvents[0].title)
                eventRepository.setUpcomingEvents(listOfEvents)
            }

        })
    }
}