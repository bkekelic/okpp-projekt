package com.ferit.okpp.eventor.model

data class User(
    val id: String = "",
    val name: String = "",
    val surname: String = "",
    val place: String = ""
)