package com.ferit.okpp.eventor.utils

import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.model.Event
import com.ferit.okpp.eventor.model.User

typealias EventDetailsClickListener = (event: EventDB) -> Unit
typealias GuestClickListener = (guest: User) -> Unit