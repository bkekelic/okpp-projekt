package com.ferit.okpp.eventor.repository

import com.ferit.okpp.eventor.model.User

class GuestsRepository {

    companion object {
        var listOfGuests = mutableListOf<User>()
    }

    fun setGuestList(guests: MutableList<User>){
        listOfGuests = guests
    }

    fun getGuestList(): MutableList<User> = listOfGuests
}