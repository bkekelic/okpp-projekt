package com.ferit.okpp.eventor.model

import java.util.*
import kotlin.collections.HashMap

data class Event(
    val uid: String = "",
    val id: String = "",
    val title: String = "",
    val location: String = "",
    val dateStarting: String = "",
    val timeString: String = "",
    var occupiedSeats: String = "",
    val totalSeats: String = "",
    val description: String = "",
    val guests: HashMap<String, String> = hashMapOf()
)

data class SomeID(
    val id: String = ""
)

