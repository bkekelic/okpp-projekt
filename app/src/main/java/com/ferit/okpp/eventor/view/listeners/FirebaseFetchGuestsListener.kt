package com.ferit.okpp.eventor.view.listeners

import com.ferit.okpp.eventor.model.User

interface FirebaseFetchGuestsListener {
    fun onSuccessUserFetch(guests: MutableList<String>)
    fun onFailureUserFetch(message: String)
}