package com.ferit.okpp.eventor.view.fragments.home

import android.content.Context
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ferit.okpp.eventor.DataAccess.NearbyEvents
import com.ferit.okpp.eventor.DataAccess.UpcomongEvents
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.MyPreference
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.repository.EventDetailsRepository
import com.ferit.okpp.eventor.repository.HomePageRepository
import com.ferit.okpp.eventor.utils.showFragment
import com.ferit.okpp.eventor.view.adapters.HomePageEventsAdapter
import com.ferit.okpp.eventor.view.fragments.BaseFragment
import com.ferit.okpp.eventor.view.listeners.ReadDataListener
import kotlinx.android.synthetic.main.fragment_home_page.*


class HomePage : BaseFragment() {

    private val nearbyEventsAdapter by lazy { HomePageEventsAdapter(::onEventClicked) }
    private val upcomingEventsAdapter by lazy { HomePageEventsAdapter(::onEventClicked) }
    private val eventDetailsRepository = EventDetailsRepository()
    private val comingEvents = mutableListOf<EventDB>()
    private val nearEvents = mutableListOf<EventDB>()

    override fun getLayoutResourceId(): Int = R.layout.fragment_home_page

    override fun setupUi() {
        nearbyEventsRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        nearbyEventsRecyclerView.adapter = nearbyEventsAdapter
        upcomingEventsRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        upcomingEventsRecyclerView.adapter = upcomingEventsAdapter

        progressBar?.visibility = View.VISIBLE

        val upcomingEvents = UpcomongEvents()
        upcomingEvents.getUpcomingEvents(object : ReadDataListener {
            override fun readData(list: MutableList<EventDB>) {

                for(e in list) {
                    Log.d("JASAM", e.location)
                    comingEvents.add(e)
                }
                setUpcomingEvents(comingEvents)
            }
        })

        val pref = MyPreference(MyApplication.ApplicationContext)
        val city = pref.getPlace()


        val nearbyEvents = NearbyEvents()
        nearbyEvents.getNearbyPlaces(city!!, object : ReadDataListener {
            override fun readData(list: MutableList<EventDB>) {
                for(e in list) {
                    Log.d("JASAM", e.dateStarting)
                    Log.d("'''", e.location)
                    nearEvents.add(e)
                }
                setNearbyEvents(nearEvents)
            }
        })

    }

    override fun onStart() {
        super.onStart()
        nearEvents.clear()
        comingEvents.clear()
    }

    override fun setUpTitle(): String = TITLE
    override fun setUpNavigationIcon(): Int? = null
    override fun setOnClickListeners() {
        addEventButton.setOnClickListener { onAddEventButtonClicked() }
    }

    private fun setUpcomingEvents(events: MutableList<EventDB>) {
        upcomingEventsAdapter.setEvents(events)
        nearbyTitleText?.text = UPCOMING_EVENTS_TITLE
        upcomingTitleText?.text = NEARBY_EVENTS_TITLE
        progressBar?.visibility = View.GONE
    }

    private fun setNearbyEvents(events: MutableList<EventDB>){
        nearbyEventsAdapter.setEvents(events)
    }

    private fun onEventClicked(event: EventDB) {
        eventDetailsRepository.onEventSelected(event)
        activity?.showFragment(R.id.eventsFragmentContainer, EventDetails.newInstance())
    }

    private fun onAddEventButtonClicked() {
        activity?.showFragment(R.id.eventsFragmentContainer, AddEvent.newInstance())
    }

    companion object {
        private const val TITLE = "Eventor"
        private const val UPCOMING_EVENTS_TITLE = "Nearby events"
        private const val NEARBY_EVENTS_TITLE = "Upcoming events"

        fun newInstance(): Fragment {
            return HomePage()
        }
    }
}
