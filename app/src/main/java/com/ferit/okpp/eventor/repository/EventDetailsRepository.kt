package com.ferit.okpp.eventor.repository

import com.ferit.okpp.eventor.DataLibrary.model.EventDB

class EventDetailsRepository {

    companion object {
        private var selectedEvent: EventDB = EventDB()
    }

    fun onEventSelected(event: EventDB) {
        selectedEvent = event
    }

    fun getSelectedEvent(): EventDB = selectedEvent
}