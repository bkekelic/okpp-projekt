package com.ferit.okpp.eventor.model.firebase

import com.ferit.okpp.eventor.model.Event
import com.ferit.okpp.eventor.model.User
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import io.reactivex.Completable
import io.reactivex.Observable

class FirebaseSource {

    fun getUserById(userId: String) = Observable.create<User> { emitter ->

        val ref = FirebaseDatabase.getInstance().getReference("users/$userId")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                if (!emitter.isDisposed)
                    emitter.onError(p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (!emitter.isDisposed) {
                    val user = p0.getValue(User::class.java)
                    if (user != null)
                        emitter.onNext(user)
                    else
                        emitter.onError(Throwable("No user found"))
                }
            }
        })
    }

    fun updateUserDetails(user: User) = Completable.create { emitter ->
        val ref = FirebaseDatabase.getInstance().getReference("users/${user.id}")
        ref.setValue(user).addOnCompleteListener {
            if (!emitter.isDisposed) {
                if (it.isSuccessful)
                    emitter.onComplete()
                else
                    emitter.onError(it.exception!!)
            }
        }
    }

    fun getUserOnGoingEvents() = Observable.create<Event> { emitter ->
        val ref = FirebaseDatabase.getInstance().getReference("events")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                if (!emitter.isDisposed)
                    emitter.onError(p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (!emitter.isDisposed) {
                    try {
                        for (snapshot in p0.children) {
                            val dbEvent = snapshot.getValue(Event::class.java) ?: Event()
                            emitter.onNext(dbEvent)
                        }
                        emitter.onComplete()
                    } catch (e: Exception) {
                        emitter.onError(e)
                    }
                }
            }
        })
    }

    fun getUserOnPastEvents() = Observable.create<Event> { emitter ->
        val ref = FirebaseDatabase.getInstance().getReference("events")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                if (!emitter.isDisposed)
                    emitter.onError(p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (!emitter.isDisposed) {
                    try {
                        for (snapshot in p0.children) {
                            val dbEvent = snapshot.getValue(Event::class.java) ?: Event()
                            emitter.onNext(dbEvent)
                        }
                        emitter.onComplete()

                    } catch (e: Exception) {
                        emitter.onError(e)
                    }

                }
            }
        })
    }


}

