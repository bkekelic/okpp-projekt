package com.ferit.okpp.eventor.view.fragments.profile

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.model.Event
import com.ferit.okpp.eventor.repository.EventDetailsRepository
import com.ferit.okpp.eventor.utils.shortToast
import com.ferit.okpp.eventor.utils.showFragment
import com.ferit.okpp.eventor.view.adapters.ProfileOnGoingEventsAdapter
import com.ferit.okpp.eventor.view.fragments.home.EventDetails
import com.ferit.okpp.eventor.view.listeners.EventListener
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchUserOnGoingEventsListener
import com.ferit.okpp.eventor.viewmodel.ProfileViewModel
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_profile_on_going_events.*

class ProfileOnGoingEvents : Fragment(), FirebaseFetchUserOnGoingEventsListener {
    lateinit var profileViewModel: ProfileViewModel
    val eventDetailsRepository = EventDetailsRepository()
    lateinit var profileOnGoingEventsAdapter: ProfileOnGoingEventsAdapter

    companion object {
        fun newInstance(): ProfileOnGoingEvents {
            return ProfileOnGoingEvents()
        }
    }

    override fun onAttach(context: Context) {
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile_on_going_events, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        profileOnGoingEventsList.layoutManager =
            LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
        profileOnGoingEventsList.itemAnimator = DefaultItemAnimator()
        /*
        profileOnGoingEventsList.addItemDecoration(
            DividerItemDecoration(
                this.context,
                0
            )
        )*/
        profileViewModel.firebaseFetchUserOnGoingEventsListener = this
        initializeAdapter()

        profileViewModel.getUserOnGoingEvents()

    }

    private fun initializeAdapter() {
        val eventListener = object : EventListener {
            override fun onClick(event: EventDB) {
                eventDetailsRepository.onEventSelected(event)
                activity?.showFragment(R.id.eventsFragmentContainer, EventDetails.newInstance())
            }

        }
        profileOnGoingEventsAdapter = ProfileOnGoingEventsAdapter(eventListener)
        profileOnGoingEventsList.adapter = profileOnGoingEventsAdapter
    }

    override fun onSuccessNextUserOnGoingEventFetch(event: Event) {
        var count = 0
        for(guest in event.guests.values) count++
        event.occupiedSeats = count.toString()

        val eventDB = EventDB(
            id = event.id,
            uid = event.uid,
            title = event.title,
            location = event.location,
            dateStarting = event.dateStarting,
            timeString = event.timeString,
            occupiedSeats = event.occupiedSeats,
            description = event.description,
            totalSeats = event.totalSeats
        )

        profileOnGoingEventsAdapter.addEvent(eventDB)
    }

    override fun onFailureUserOnGoingEventsFetch(message: String) {
        context?.shortToast("Oops some error happened")
        Log.e("TAG", message)
    }

    override fun onCompleteOnGoingEventsFetch() {
        Log.d("TAG", "On complete on going event")
    }

}