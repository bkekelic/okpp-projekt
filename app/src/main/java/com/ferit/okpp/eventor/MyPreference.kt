package com.ferit.okpp.eventor

import android.content.Context
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class MyPreference(context: Context) {
    val PREFERENCE_NAME = "User"
    val NAME = "name"
    val SURNAME = "surnema"
    val PLACE = "place"
    val EVENTS_LIST = "EventsList"
    val PROFILE_USER_ID = "profileUserId"
    val EVENTS: MutableList<EventDB>? = null

    val preference = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun getName(): String? {
        return preference.getString(NAME, "0")
    }
    fun getSurname(): String? {
        return preference.getString(SURNAME, "0")
    }
    fun getPlace(): String? {
        return preference.getString(PLACE, "0")
    }

    inline fun getEvents(): MutableList<EventDB>? {
        val GSON: Gson? = Gson()
        val json = preference.getString(EVENTS_LIST, "0")
        val list: MutableList<EventDB>? =  GSON?.fromJson(json, object: TypeToken<MutableList<EventDB>>(){}.type)
        return list
    }

    fun setName(cityName: String?) {
        val editor = preference.edit()
        editor.putString(NAME, cityName)
        editor.apply()
    }
    fun setSurname(cityName: String?) {
        val editor = preference.edit()
        editor.putString(SURNAME, cityName)
        editor.apply()
    }
    fun setPlace(cityName: String?) {
        val editor = preference.edit()
        editor.putString(PLACE, cityName)
        editor.apply()
    }

    fun setEvents(date: MutableList<EventDB>) {
        val editor = preference.edit()
        val GSON: Gson? = Gson()
        val json = GSON?.toJson(date)
        editor.putString(EVENTS_LIST, json)
        editor.apply()
    }

    fun setUserProfileId(userProfileId: String?){
        val editor = preference.edit().also {
            it.putString(PROFILE_USER_ID, userProfileId)
            it.apply()
        }
    }
    fun getUserProfileId() = preference.getString(PROFILE_USER_ID, "") ?: ""
}