package com.ferit.okpp.eventor.view.fragments.profile

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.lifecycle.ViewModelProviders
import com.ferit.okpp.eventor.LoginActivity
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.model.Event
import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.utils.shortToast
import com.ferit.okpp.eventor.utils.showFragment
import com.ferit.okpp.eventor.view.adapters.ProfileViewPagerAdapter
import com.ferit.okpp.eventor.view.fragments.BaseFragment
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchUserListener
import com.ferit.okpp.eventor.viewmodel.ProfileViewModel
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment(), FirebaseFetchUserListener {

    private lateinit var profileViewModel: ProfileViewModel

    private lateinit var userProfileId: String

    companion object {

        private const val USER_KEY = "user"
        private const val TITLE = "Profile"
        fun newInstance(value: String) = ProfileFragment().apply {
            arguments = Bundle().apply {
                putString(USER_KEY, value)
            }
        }
    }

    override fun setUpTitle() = TITLE

    override fun setUpNavigationIcon() = R.drawable.arrow_back_white
    override fun setOnClickListeners() {}
    override fun getLayoutResourceId(): Int = R.layout.fragment_profile
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getString(USER_KEY)?.let {
            userProfileId = it
        }
    }

    override fun setupUi() {
        initListeners()
        initViewModel()
        initViewPager()
        profileViewModel.getUserDetails(userProfileId)
    }

    private fun initListeners() {
        profilePicture.setOnClickListener {}
    }

    private fun initViewModel() {
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        profileViewModel.firebaseFetchUserListener = this
    }

    private fun initViewPager() {
        profileViewPager.adapter = ProfileViewPagerAdapter(childFragmentManager)
        profileTabLayout.setupWithViewPager(profileViewPager)
    }


    override fun onSuccessUserFetch(user: User) {
        showUserDetails(user)
    }

    override fun onFailureUserFetch(message: String) {
        context?.shortToast("Oops some error happened")
        Log.e("TAG", message)
    }
    private fun showEditProfileFragment() {
        activity!!.showFragment(
            R.id.eventsFragmentContainer,
            EditProfileFragment.newInstance(userProfileId)
        )
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        if (checkIfUserIsInHisProfile())
            inflater.inflate(R.menu.profile_hamburger, menu)
        else
            inflater.inflate(R.menu.home_page_icon_menu, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_editProfile -> showEditProfileFragment()
            R.id.menu_signOut -> signOut()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun checkIfUserIsInHisProfile(): Boolean {
        return (FirebaseAuth.getInstance().uid == userProfileId)
    }

    private fun showUserDetails(userToShow: User) {
        profileNameSurname.text = "${userToShow.name} ${userToShow.surname}"
        profileUserPlace.text = userToShow.place
    }

    private fun signOut() {
        FirebaseAuth.getInstance().signOut()
        val intent = Intent(activity, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}