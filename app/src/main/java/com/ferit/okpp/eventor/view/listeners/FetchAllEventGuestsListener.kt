package com.ferit.okpp.eventor.view.listeners

import com.ferit.okpp.eventor.model.User

interface FetchAllEventGuestsListener {
    fun onSuccessGuestsFetch(guests: MutableList<User>)
}