package com.ferit.okpp.eventor.utils

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

fun getDateAsString(date: Date): String {
    val sdf = SimpleDateFormat("dd.MM.yyyy.")
    return sdf.format(date)
}

fun getLocalDateAsString(date: LocalDate): String {
    val sdf = SimpleDateFormat("dd.MM.yyyy.")
    return sdf.format(date)
}

fun getTimeAsString(date: Date): String {
    val sdf = SimpleDateFormat("HH:mm")
    return sdf.format(date)
}

fun formatDateString(date: String): String{
    val strs = date.split("-").toTypedArray()
    return "${strs[2]}.${strs[1]}.${strs[0]}."
}