package com.ferit.okpp.eventor.repository

import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.model.firebase.FirebaseSource

class ProfileRepository {
    var name: String = ""
    var surname: String = ""
    var place: String = ""

    private val firebase: FirebaseSource = FirebaseSource()

    fun getUserDetails(userId: String) = firebase.getUserById(userId)

    fun updateUser(user: User) = firebase.updateUserDetails(user)

    fun getUserOnGoingEvents(userId: String) = firebase.getUserOnGoingEvents()

    fun getUserPastEvents(userId: String) = firebase.getUserOnPastEvents()

}