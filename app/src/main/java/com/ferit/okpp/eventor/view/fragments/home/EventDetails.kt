package com.ferit.okpp.eventor.view.fragments.home

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.ferit.okpp.eventor.DataAccess.ReserveASeat
import com.ferit.okpp.eventor.DataAccess.UserCount
import com.ferit.okpp.eventor.DataAccess.UserInformation
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.repository.EventDetailsRepository
import com.ferit.okpp.eventor.repository.GuestsRepository
import com.ferit.okpp.eventor.repository.ProfileRepository
import com.ferit.okpp.eventor.utils.formatDateString
import com.ferit.okpp.eventor.utils.showFragment
import com.ferit.okpp.eventor.view.fragments.BaseFragment
import com.ferit.okpp.eventor.view.fragments.profile.ProfileFragment
import com.ferit.okpp.eventor.view.listeners.FetchAllEventGuestsListener
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchGuestsListener
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchUserListener
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_event_details.*

class EventDetails : BaseFragment() {

    private val eventDetailsRepository = EventDetailsRepository().getSelectedEvent()
    private val guestsRepository = GuestsRepository()
    private var userProfileId = ""
    private var guestList: MutableList<String> = mutableListOf()
    private var usersInformation = mutableListOf<User>()
    private var eventUserCounter = 0

    override fun getLayoutResourceId(): Int = R.layout.fragment_event_details

    override fun setupUi() {
        eventTitle.text = eventDetailsRepository.title
        eventDateTitle.text = formatDateString(eventDetailsRepository.dateStarting)
        eventDescription.text = eventDetailsRepository.description
        eventLocationTitle.text = eventDetailsRepository.location
        getUserId()
        setAttendanceCount()
    }

    override fun setUpTitle(): String = TITLE

    override fun setOnClickListeners() {
        attendButton.setOnClickListener{ onAttendButtonClicked() }
        eventAttendanceTitle.setOnClickListener { onGuestsClicked() }
    }

    private fun onGuestsClicked() {
        val dialog = GuestsDialog.newInstance()
        Log.d("999", guestList[0])
        guestsRepository.setGuestList(usersInformation)
        dialog.show(childFragmentManager, dialog.tag)
    }

    private fun getUserId() {
        userProfileId = FirebaseAuth.getInstance().currentUser!!.uid
        Log.d("77777", FirebaseAuth.getInstance().currentUser!!.uid)
    }

    private fun setAttendanceCount() {
        val c = UserCount()
        var userCounter = 0

        c.userCount(eventDetailsRepository.id, object : FirebaseFetchGuestsListener{
            override fun onSuccessUserFetch(guests: MutableList<String>) {
                for(u in guests) {
                    Log.d("EventGuests", u)
                    if (u == userProfileId){
                        attendButton?.setVisibility(View.INVISIBLE)
                        attendingTitleText.text = ATTENDING_TITLE
                    }
                    userCounter++
                }
                eventSeatTaken.text = userCounter.toString() + "/" + eventDetailsRepository.totalSeats + " places reserved"
                for(g in guests) {
                    guestList.add(g)
                }
                eventUserCounter = userCounter
                Log.d("7755", eventUserCounter.toString())
                setUpGuestList(guestList, userCounter)
            }
            override fun onFailureUserFetch(message: String) {
            }
        })
    }

    private fun setUpGuestList(guests: MutableList<String>, userCounter: Int) {
        val users = UserInformation()
        var guestsString = ""
        users.getUserInformation(guests, object : FetchAllEventGuestsListener{
            override fun onSuccessGuestsFetch(guests: MutableList<User>) {
                usersInformation = guests
                if (userCounter == 0) eventAttendanceTitle.text = "No guests signed up"
                else if (userCounter <= 5){
                    var i = 0
                    while (i < userCounter){
                        if (i == userCounter - 1){
                            guestsString += guests[i].name
                            break
                        }
                        guestsString += guests[i].name + ", "
                        i++
                    }
                    eventAttendanceTitle.text = guestsString + " signed up"
                }
                else{
                    var i = 0
                    while (i <5){
                        guestsString += guests[i].name + ", "
                        i++
                    }
                    Log.d("trenutno", userCounter.toString())
                    var temp = userCounter - 5
                    eventAttendanceTitle?.text = guestsString + "and $temp more are coming"
                }

            }

        })
    }

    private fun onAttendButtonClicked() {
        val userId = FirebaseAuth.getInstance().currentUser!!.uid
        val reserveASeat = ReserveASeat()
        if (eventUserCounter < eventDetailsRepository.totalSeats.toInt()){
            reserveASeat.reserveASeat(eventDetailsRepository.id, userId)
        }else{
            Toast.makeText(MyApplication.ApplicationContext, "Event is full", Toast.LENGTH_SHORT).show()
        }
    }

    override fun setUpNavigationIcon(): Int? = R.drawable.arrow_back_white

    companion object {
        private const val TITLE = "Event details"
        private const val USER_KEY = "user"
        private const val ATTENDING_TITLE = "You are attending this event"
        fun newInstance(): Fragment {
            return EventDetails()
        }
    }
}
