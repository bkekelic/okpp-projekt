package com.ferit.okpp.eventor.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.utils.EventDetailsClickListener
import com.ferit.okpp.eventor.utils.formatDateString
import kotlinx.android.synthetic.main.item_event_summary.view.*

class HomePageEventsAdapter(private val eventDetailsClickListener: EventDetailsClickListener): RecyclerView.Adapter<EventsHolder>() {

    private val events: MutableList<EventDB> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_event_summary, parent, false)
        return EventsHolder(view)
    }

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: EventsHolder, position: Int) {
        holder.bindData(events[position], eventDetailsClickListener)
    }

    fun setEvents(events: MutableList<EventDB>){
        this.events.clear()
        this.events.addAll(events)
        notifyDataSetChanged()
    }

    fun refreshData(){
        notifyDataSetChanged()
    }
}

class EventsHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    fun bindData(event: EventDB, eventDetailsClickListener: EventDetailsClickListener){
        itemView.setOnClickListener { eventDetailsClickListener(event) }
        itemView.eventTitle.text = event.title
        itemView.eventLocationText.text = event.location
        itemView.eventDateText.text = formatDateString(event.dateStarting)
        itemView.eventAttendanceText.text = event.totalSeats
    }
}