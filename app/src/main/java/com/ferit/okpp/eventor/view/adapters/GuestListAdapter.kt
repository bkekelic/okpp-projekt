package com.ferit.okpp.eventor.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.utils.GuestClickListener
import kotlinx.android.synthetic.main.item_guest.view.*

class GuestListAdapter(private val guestClickListener: GuestClickListener): RecyclerView.Adapter<GuestsHolder>() {

    private val guests: MutableList<User> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuestsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_guest, parent, false)
        return GuestsHolder(view)
    }

    override fun getItemCount(): Int = guests.size

    override fun onBindViewHolder(holder: GuestsHolder, position: Int) {
        holder.bindData(guests[position], guestClickListener)
    }

    fun setGuests(events: MutableList<User>){
        this.guests.clear()
        this.guests.addAll(events)
        notifyDataSetChanged()
    }
}

class GuestsHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    fun bindData(guest: User, guestClickListener: GuestClickListener){
        itemView.guestNameText.setOnClickListener { guestClickListener(guest) }
        itemView.guestNameText.text = guest.name + " " + guest.surname
    }
}