package com.ferit.okpp.eventor.DataAccess

import android.util.Log
import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchGuestsListener
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchUserListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class UserCount {

    lateinit var guestsList: MutableList<String>

    fun userCount(eventId: String, fetchGuestsListener: FirebaseFetchGuestsListener) {
        val ref = FirebaseDatabase.getInstance().getReference("events/$eventId/guests")

        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                val count = p0.childrenCount
                guestsList = mutableListOf()

                for(e in p0.children) {

                    guestsList.add(e.getValue().toString())

                    Log.d("Guests", e.toString())
                }

                fetchGuestsListener.onSuccessUserFetch(guestsList)

                Log.d("Count", count.toString())
            }
        })
    }
}