package com.ferit.okpp.eventor.repository

import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.MyPreference
import com.ferit.okpp.eventor.model.Event

class HomePageRepository {

    companion object {
        var listOfEvents = mutableListOf<EventDB>()
    }

    fun setUpcomingEvents(events: MutableList<EventDB>){
        listOfEvents = events
    }

    fun getNearbyEvents(): MutableList<EventDB> = listOfEvents

    fun getUpcomingEvents(): MutableList<EventDB> = listOfEvents

}