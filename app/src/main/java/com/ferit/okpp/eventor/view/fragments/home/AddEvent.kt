package com.ferit.okpp.eventor.view.fragments.home

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.utils.getTimeAsString
import com.ferit.okpp.eventor.utils.showFragment
import com.ferit.okpp.eventor.view.activities.HomePageActivity
import com.ferit.okpp.eventor.view.fragments.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_add_event.*
import kotlinx.android.synthetic.main.fragment_event_details.*
import java.util.*

class AddEvent : BaseFragment() {

    private val calendar = Calendar.getInstance()
    private val year = calendar.get(Calendar.YEAR)
    private val month = calendar.get(Calendar.MONTH)
    private val day = calendar.get(Calendar.DAY_OF_MONTH)
    lateinit var date: String

    override fun getLayoutResourceId(): Int = R.layout.fragment_add_event

    override fun setUpTitle(): String = TITLE

    override fun setupUi() {
        Log.d("1111", year.toString())
        Log.d("1111", month.toString())
        Log.d("1111", day.toString())
    }
    override fun setOnClickListeners() {
        eventDateText.setOnClickListener { openCalendarDialog() }
        createEventButton.setOnClickListener{ addEventToFirebase() }
        eventTimeText.setOnClickListener{ openTimePicker() }
    }

    private fun openTimePicker() {

        val timeSetListener = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
            calendar.set(Calendar.MINUTE, minute)
            eventTimeText.setText(getTimeAsString(calendar.time))
        }

        TimePickerDialog(context, timeSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show()
    }

    private fun addEventToFirebase() {

        when{
            eventTitleEditText?.text.toString() == "" -> {makeToast(EMPTY_TITLE); return}
            eventLocationEditText?.text.toString() == "" -> {makeToast(EMPTY_LOCATION); return}
            eventLimitEditText?.text.toString() == "" -> {makeToast(EMPTY_LIMIT); return}
            eventLimitEditText?.text.toString() == "0" -> {makeToast(WRONG_LIMIT); return}
            eventDateText.text.toString() == "Pick a date" -> {makeToast(EMPTY_DATE); return}
            eventTimeText.text.toString() == "Pick time" -> {makeToast(EMPTY_TIME); return}

        }

        val id = UUID.randomUUID().toString()
        val uid = FirebaseAuth.getInstance().currentUser!!.uid

        val ref = FirebaseDatabase.getInstance().getReference("events/$id")



        val event = EventDB(uid,
            id,
            eventTitleEditText.text.toString(),
            eventLocationEditText.text.toString(),
            date,
            eventTimeText.text.toString(),
            0.toString(),
            eventLimitEditText.text.toString(),
            eventDescriptionEditText.text.toString())

        ref.setValue(event)
            .addOnSuccessListener {
                Log.d("AddEvent", "Event successfully saved to Firebase.")

                activity?.showFragment(R.id.eventsFragmentContainer, HomePage.newInstance())
            }
            .addOnFailureListener{
                Log.d("AddEvent", "Failed to save event to databse.")
            }
    }

    private fun openCalendarDialog() {
        val datePickerDialog = DatePickerDialog(context as HomePageActivity,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                if (dayOfMonth < 10) {
                    date = "$year-${monthOfYear + 1}-0$dayOfMonth"
                } else if (monthOfYear < 10) {
                    date = "$year-0${monthOfYear + 1}-$dayOfMonth"
                } else {
                    date = "$year-${monthOfYear + 1}-$dayOfMonth"
                }

                /*if(monthOfYear < 10 && dayOfMonth < 10) {
                    date = "$year-0${monthOfYear+1}-0$dayOfMonth"
                }
                else {
                    date = "$year-${monthOfYear+1}-$dayOfMonth"
                }*/
                Log.d("add", date)
                eventDateText.setText("" + dayOfMonth + "." + (monthOfYear + 1) + "." + year)
            }, year, month, day
        ).also {
            it.datePicker.minDate = System.currentTimeMillis() - 1000
            it.show()
        }
    }

    override fun setUpNavigationIcon(): Int? = R.drawable.arrow_back_white

    private fun makeToast(message: String){
        Toast.makeText(MyApplication.ApplicationContext, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val TITLE = "Add event"
        private const val EMPTY_TITLE = "Please enter title"
        private const val EMPTY_LOCATION = "Please enter location"
        private const val EMPTY_LIMIT = "Please enter limit"
        private const val WRONG_LIMIT = "Limit has to be one or more"
        private const val EMPTY_DATE = "Please pick a date"
        private const val EMPTY_TIME = "Please select time"
        fun newInstance(): Fragment {
            return AddEvent()
        }
    }
}
