package com.ferit.okpp.eventor.view.activities

import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import com.ferit.okpp.eventor.DataAccess.NearbyEvents
import com.ferit.okpp.eventor.DataAccess.UpcomongEvents
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.MyPreference
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.RegistrationActivity
import com.ferit.okpp.eventor.utils.showFragment
import com.ferit.okpp.eventor.view.fragments.home.HomePage
import com.ferit.okpp.eventor.view.listeners.ReadDataListener
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class HomePageActivity : BaseActivity(), OnMapReadyCallback {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val PLACE_PICKER_REQUEST = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(MyApplication.ApplicationContext)
        MapReady()

        verifyUserIsLoggedIn()

        Log.d("WhereIAm", "Bok")
    }

    private fun verifyUserIsLoggedIn() {
        val uid = FirebaseAuth.getInstance().uid

        if (uid == null) {
            val intent = Intent(this, RegistrationActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }else{
            setupUi()
            initializeToolbar()
        }
    }

    private fun setupUi() {
        showFragment(containerId = R.id.eventsFragmentContainer, shouldAddToBackStack = false,fragment =  HomePage.newInstance())

    }

    private fun initializeToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.title_homePage)
    }

    override fun onMapReady(p0: GoogleMap?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun MapReady() {
        setUpMap()
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                val city = geoLocate(currentLatLng)
                Log.d("WhereIAm", currentLatLng.toString())
            }
        }
    }

    private fun geoLocate(latLng: LatLng): String {
        val geocoder = Geocoder(this)
        val addresses: List<Address>?
        val address: Address?
        var city = ""

        addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)

        if (addresses.size > 0) {
            address = addresses.get(0)
            city += address.locality

            Log.d("WhereIAm", city.toString())
        }

        val pref = MyPreference(this)
        pref.setPlace(city)

        return city
    }

    override fun onResume() {
        super.onResume()
    }
}
