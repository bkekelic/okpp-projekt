package com.ferit.okpp.eventor.view.activities

import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.ferit.okpp.eventor.MyApplication
import com.ferit.okpp.eventor.MyPreference
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.utils.showFragment
import com.ferit.okpp.eventor.view.fragments.home.HomePage
import com.ferit.okpp.eventor.view.fragments.profile.ProfileFragment
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseActivity : AppCompatActivity() {

    private var disposables: CompositeDisposable = CompositeDisposable()
    private val pref = MyPreference(MyApplication.ApplicationContext)


    @Synchronized
    protected fun addDisposable(disposable: Disposable?) {
        if (disposable == null) return
        disposables.add(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!disposables.isDisposed) disposables.dispose()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_homePage -> goToHomePage()
            R.id.profile_icon -> showProfileActivity()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun goToHomePage() {
        showFragment(R.id.eventsFragmentContainer, HomePage.newInstance())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.profile_icon_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /*
    * This is how you call profile activity
    * Change uid for every user
     */
    private fun showProfileActivity(){
        val uid = FirebaseAuth.getInstance().uid
        pref.setUserProfileId(uid)
        showFragment(R.id.eventsFragmentContainer, ProfileFragment.newInstance(uid ?: ""))
    }



}