package com.ferit.okpp.eventor.view.listeners

import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.model.Event
import com.ferit.okpp.eventor.model.User

interface ReadDataListener {
    fun readData(list: MutableList<EventDB>)
}