package com.ferit.okpp.eventor.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.utils.formatDateString
import com.ferit.okpp.eventor.view.listeners.EventListener
import kotlinx.android.synthetic.main.item_event_summary.view.*

class ProfilePastEventsAdapter(eventListener: EventListener) :
    RecyclerView.Adapter<PastEventHolder>() {

    private var events: MutableList<EventDB>
    private val eventListener: EventListener

    init {
        this.events = mutableListOf()
        this.eventListener = eventListener
    }

    fun addEvent(event: EventDB) {
        events.add(event)
        notifyItemInserted(events.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PastEventHolder {
        val eventView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_event_summary, parent, false)
        return PastEventHolder(eventView)
    }

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: PastEventHolder, position: Int) {
        holder.bind(events[position], eventListener)
    }

}

class PastEventHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(event: EventDB, eventListener: EventListener) {

        itemView.layoutParams = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        itemView.eventTitle.text = event.title
        itemView.eventDateText.text = formatDateString(event.dateStarting) + " " + event.timeString
        itemView.eventLocationText.text = event.location
        itemView.eventAttendanceText.text = event.occupiedSeats.toString()

        itemView.setOnClickListener { eventListener.onClick(event) }
    }
}
