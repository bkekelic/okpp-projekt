package com.ferit.okpp.eventor.view.listeners

import com.ferit.okpp.eventor.DataLibrary.model.EventDB
import com.ferit.okpp.eventor.model.Event

interface FirebaseFetchUserOnGoingEventsListener {
    fun onSuccessNextUserOnGoingEventFetch(event: Event)
    fun onFailureUserOnGoingEventsFetch(message: String)
    fun onCompleteOnGoingEventsFetch()
}