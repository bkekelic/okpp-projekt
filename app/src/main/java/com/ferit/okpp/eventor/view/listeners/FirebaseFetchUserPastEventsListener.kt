package com.ferit.okpp.eventor.view.listeners

import com.ferit.okpp.eventor.model.Event

interface FirebaseFetchUserPastEventsListener {
    fun onSuccessNextUserPastEventFetch(event: Event)
    fun onFailureUserPastEventsFetch(message: String)
    fun onCompletePastEventsFetch()
}