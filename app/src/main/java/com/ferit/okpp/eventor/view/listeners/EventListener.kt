package com.ferit.okpp.eventor.view.listeners

import com.ferit.okpp.eventor.DataLibrary.model.EventDB

interface EventListener {
    fun onClick(event: EventDB)
}