package com.ferit.okpp.eventor.DataLibrary.model

import com.ferit.okpp.eventor.model.User

class EventDB(
    val uid: String = "",
    val id: String = "",
    val title: String = "",
    val location: String = "",
    val dateStarting: String = "",
    val timeString: String = "",
    val occupiedSeats: String = "",
    val totalSeats: String = "",
    val description: String = ""
)