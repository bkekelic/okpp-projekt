package com.ferit.okpp.eventor.view.fragments.profile

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.ferit.okpp.eventor.R
import com.ferit.okpp.eventor.model.User
import com.ferit.okpp.eventor.utils.shortToast
import com.ferit.okpp.eventor.view.fragments.BaseFragment
import com.ferit.okpp.eventor.view.listeners.FirebaseFetchUserListener
import com.ferit.okpp.eventor.view.listeners.FirebaseUpdateUserListener
import com.ferit.okpp.eventor.viewmodel.ProfileViewModel
import kotlinx.android.synthetic.main.fragment_edit_profile.*

class EditProfileFragment : BaseFragment(), FirebaseFetchUserListener, FirebaseUpdateUserListener {
    private lateinit var profileViewModel: ProfileViewModel

    private var userProfileId: String = ""

    companion object {
        private const val USER_KEY = "user"

        private const val TITLE = "Edit profile"
        fun newInstance(value: String) = EditProfileFragment().apply {
            arguments = Bundle().apply {
                putString(USER_KEY, value)
            }
        }
    }

    override fun setUpTitle(): String = TITLE
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getString(USER_KEY)?.let {
            userProfileId = it
        }
    }

    override fun onSuccessUserFetch(user: User) {
        fillWithUserData(user)
    }

    override fun onFailureUserFetch(message: String) {
        context?.shortToast("Oops some error happened")
        Log.e("TAG", message)
    }

    override fun onSuccessUserUpdate() {
        context?.shortToast("Profile updated successfully")
        fragmentManager?.popBackStack()
    }

    override fun onFailureUserUpdate(message: String) {
        context?.shortToast("Oops can't update user details")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(R.menu.save_edit_profile_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_saveEditProfile -> updateUserDetails()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getLayoutResourceId(): Int = R.layout.fragment_edit_profile
    override fun setOnClickListeners() {}
    override fun setUpNavigationIcon(): Int = R.drawable.ic_close_white

    override fun setupUi() {
        initViewModel()
        profileViewModel.getUserDetails(userProfileId)
    }

    private fun initViewModel() {
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        profileViewModel.firebaseFetchUserListener = this
        profileViewModel.firebaseUpdateUserListener = this
    }

    private fun fillWithUserData(user: User) {
        user.let {
            editProfile_name.setText(it.name)
            editProfile_surname.setText(it.surname)
            editProfile_place.setText(it.place)
        }
    }

    private fun updateUserDetails() {
        if (checkInputFields()) {
            User(
                id = userProfileId,
                name = editProfile_name.text.toString(),
                surname = editProfile_surname.text.toString(),
                place = editProfile_place.text.toString()
            ).apply {
                profileViewModel.updateUser(this)
            }
        } else Toast.makeText(
            context,
            getString(R.string.editProfile_messageEmptyField),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun checkInputFields(): Boolean {
        return !(editProfile_name.text.toString().isEmpty() ||
                editProfile_surname.text.toString().isEmpty() ||
                editProfile_place.text.toString().isEmpty())
    }
}