package com.ferit.okpp.eventor.view.listeners

import com.ferit.okpp.eventor.model.User

interface FirebaseUpdateUserListener {
    fun onSuccessUserUpdate()
    fun onFailureUserUpdate(message: String)
}